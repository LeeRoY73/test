import { createApp } from 'vue'
import App from './App.vue'
import { registerPlugins } from '@/plugins'

// Vuetify
import 'vuetify/styles'
import { createVuetify } from 'vuetify'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'
import store from './store'
import router from './router'

const vuetify = createVuetify({
    components,
    directives,
})

const app = createApp(App).use(router).use(store).use(store).use(vuetify)

registerPlugins(app)

app.mount('#app')

